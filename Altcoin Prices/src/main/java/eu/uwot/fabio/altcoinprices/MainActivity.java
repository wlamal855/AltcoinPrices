package eu.uwot.fabio.altcoinprices;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String URL0 = "<!-- TradingView Widget BEGIN -->\n" +
            "<script type=\"text/javascript\" src=\"https://s3.tradingview.com/tv.js\"></script>\n" +
            "<script type=\"text/javascript\">\n" +
            "new TradingView.widget({\n" +
            "  \"autosize\": true,\n" +
            "  \"symbol\": \"KRAKEN:";
    String URL1 = "\",\n" +
            "  \"interval\": ";
    String URL2 = ",\n" +
            "  \"timezone\": \"Europe/Rome\",\n" +
            "  \"theme\": \"Light\",\n" +
            "  \"style\": \"1\",\n" +
            "  \"locale\": \"en\",\n" +
            "  \"toolbar_bg\": \"#f1f3f6\",\n" +
            "  \"enable_publishing\": false,\n" +
            "  \"withdateranges\": true,\n" +
            "  \"save_image\": false,\n" +
            "  \"hideideas\": true,\n" +
            "  \"studies\": [\n" +
            "    \"BB@tv-basicstudies\",\n" +
            "    \"RSI@tv-basicstudies\"\n" +
            "  ]\n" +
            "});\n" +
            "</script>\n" +
            "<!-- TradingView Widget END -->\n";
    String URL05 = "<!-- TradingView Widget BEGIN -->\n" +
            "<script type=\"text/javascript\" src=\"https://s3.tradingview.com/tv.js\"></script>\n" +
            "<script type=\"text/javascript\">\n" +
            "new TradingView.widget({\n" +
            "  \"autosize\": true,\n" +
            "  \"symbol\": \"BITFINEX:";
    String currency;
    String altcoin;
    String period;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        /* load  user settings */
        SharedPreferences prefs =getApplicationContext().getSharedPreferences("Settings", 0);
        SharedPreferences.Editor editor = prefs.edit();
        this.currency = prefs.getString("currency", "EUR");
        this.altcoin = prefs.getString("altcoin", "XMR");
        this.period = prefs.getString("period", "30");
        editor.commit();
        /* end load user settings */

        /* data from kraken.com */
        String url;
        if (this.altcoin.equals("XBTUSD")) {
            url = URL0 + this.altcoin + "" + URL1 + this.period + URL2;
        } else if (this.altcoin.equals("BTCUSD")) {
            url = URL05 + this.altcoin + "" + URL1 + this.period + URL2;
        } else {
            url = URL0 + this.altcoin + this.currency + URL1 + this.period + URL2;
        }

        WebView view = (WebView) this.findViewById(R.id.webView);
        view.getSettings().setJavaScriptEnabled(true);
        view.getSettings().setDomStorageEnabled(true);
        view.loadData(url, null, null);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_xbtusd) {
            saveLastState("XBTUSD");
            String url = URL0 + "XBTUSD" + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_btcusd) {
            saveLastState("BTCUSD");
            String url = URL05 + "BTCUSD" + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_xmr) {
            saveLastState("XMR");
            String url = URL0 + "XMR" + this.currency + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_xbt) {
            saveLastState("XBT");
            String url = URL0 + "XBT" + this.currency + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_bch) {
            saveLastState("BCH");
            String url = URL0 + "BCH" + this.currency + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_dash) {
            saveLastState("DASH");
            String url = URL0 + "DASH" + this.currency + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_eth) {
            saveLastState("ETH");
            String url = URL0 + "ETH" + this.currency + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_etc) {
            saveLastState("ETC");
            String url = URL0 + "ETC" + this.currency + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_ltc) {
            saveLastState("LTC");
            String url = URL0 + "LTC" + this.currency + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_rep) {
            saveLastState("REP");
            String url = URL0 + "REP" + this.currency + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_xrp) {
            saveLastState("XRP");
            String url = URL0 + "XRP" + this.currency + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_zec) {
            saveLastState("ZEC");
            String url = URL0 + "ZEC" + this.currency + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_eos) {
            saveLastState("EOS");
            String url = URL0 + "XMR" + "XBT" + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_gno) {
            saveLastState("GNO");
            String url = URL0 + "GNO" + "XBT" + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_icn) {
            saveLastState("ICN");
            String url = URL0 + "ICN" + "XBT" + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_mln) {
            saveLastState("MLN");
            String url = URL0 + "MLN" + "XBT" + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_xdg) {
            saveLastState("XDG");
            String url = URL0 + "XDG" + "XBT" + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        } else if (id == R.id.nav_xlm) {
            saveLastState("XLM");
            String url = URL0 + "XLM" + "XBT" + URL1 + this.period + URL2;
            WebView view = (WebView) this.findViewById(R.id.webView);
            view.getSettings().setJavaScriptEnabled(true);
            view.loadData(url, null, null);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean saveLastState (String altcoin) {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Settings", 0); // 0 for private mode
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("altcoin", altcoin);
        editor.commit();

        return true;
    }
}
